import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    hits: null,
    homeHits: null,
    searchResults: null,
    cartItems: [],
  },
  mutations: {
    setHits: (state, payload) => {
      state.hits = payload;
    },
    setItemInCart: (state, payload) => {
      state.cartItems.push(payload);
    },
    setHomeHits:(state, payload) => {
      state.homeHits = payload;
    },
    removeItemFromCartByIndex: (state,payload) => {
      state.cartItems.splice(payload, 1);
    },
    setSearchResults: (state,payload) => {
      if(state.hits){
        state.searchResults = state.hits.filter((hit) => {
          return hit.name.toLowerCase().includes(payload.toLowerCase()) || hit.brand.toLowerCase().includes(payload.toLowerCase()) || hit.categories.includes(payload.toLowerCase())
        });
        state.searchResults.sort((a,b) => {
          return b.popularity - a.popularity;
        });
      }
    },
    setSearchResultsWithCategory: (state,payload) => {
      if(state.hits){
        state.searchResults = state.hits.filter((hit) => {
          let brand = payload.brand;
          let category = payload.category;
          if(hit.categories.includes(category) &&
              (hit.name.toLowerCase().includes(brand.toLowerCase()) || hit.brand.toLowerCase().includes(brand.toLowerCase()) || hit.description.toLowerCase().includes(brand.toLowerCase()))){
            return hit;
          }
        });
      }
      state.searchResults.sort((a,b)=>{
        return b.popularity - a.popularity;
      });
    },
    clearCart(state){
      state.cartItems = [];
    }
  },
  getters: {
    getHits: state => {
      return state.hits;
    },
    getHomeHits: state => {
      return state.homeHits;
    },
    getCartItems: state => {
      return state.cartItems
    },
    getSearchResults: state => {
      return state.searchResults
    }
  },
  actions: {
    search(context,payload){
      context.commit('setSearchResults',payload);
    },
    searchWithCategory(context,payload){
      context.commit('setSearchResultsWithCategory',payload);
    },
    getHits: async function (context) {
      return new Promise((resolve, reject) => {
        axios.get('../data/data.json').then(result => {
          let hits = result.data.hits;
          context.commit('setHits',hits);
          context.commit('setHomeHits',hits.filter(hit => hit.popularity > 17000));
          resolve();
        }).catch(e => {
          console.log(e);
          reject(e);
        })
      });
    },
    addToCart(context,payload){
      context.commit('setItemInCart',payload);
    },
    removeItemFromCartByIndex(context,payload){
      context.commit('removeItemFromCartByIndex',payload);
    },
    clearCart(context){
      context.commit('clearCart');
    }
  },
  modules: {
  }
})
